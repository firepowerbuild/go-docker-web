1.3.3:
    Fix case with CLI when executor not specified
    Fix HTML layout
1.3.2:
    Add optional session_timeout in go-d.ini to specify duration of web sessions (in seconds)
    Do not add executor tags if already present
    Fix user info reload at login
    Set container.id in task to null when creating tasks (issue with CLI)
1.3.1:
    Fix interactive jobs. An error on web interface prevents interactive jobs
1.3:
    Add multi executor support, needs go-docker >= 1.3
    Add optional hostname to prometheus stats
    Fix prometheus + gunicorn multiprocess metrics
        Modifies startup command and needs a temp directory
1.2:
    GODWEB-4 optionally view tasks of users in same project
        New go-d.ini parameter "share_project_tasks", need to be True to view other project member tasks
    Allow proxy authentication using REMOTE_USER header
        New go-d.ini parameter allow_proxy_remote_user to allow this kind of authentication
    Remove image usage increment from web and switch it to godocker watchers, to add image only when job could be started
    Fix #20 add different label color if system error is detected (status.reason set and exitcode > 0 && != 137)
    Add link to files in list of jobs over
    GOD-51 Support CNI networks
    GOD-35 Support Mesos unified containerizer, needs go-docker >= 1.2   
    Fix #17 Script content in task info doesn't appear
    Fix #24 Resources button go to a blank page
    Fix #22 Create a "New Job" button in menu
    Fix #23 Create an advanced section in *new job* form
    Fix browse of job files
    Add recipe removal
    Add a link to images to their web page with new go-d.ini param link_image_to_registry
    Fix admin access to recipes
    Fix #21 UI improvements
    Fix refresh of live logs
    Refactor display of jobs over with remote pagination to be able to look at all previous jobs
    Fix #25 Add route prefix option with prefix parameter in development/production.ini file or env variable GODOCKER_WEB_PREFIX
1.1.2:
    GOD-45 Support linux group management in projects
    Add resource page to see used resources on cluster
    Get user quota from auth plugins
    Add possiblity to open additional ports
    GODWEB-3 add marketplace recipes
    fix logout issue
    Add dynamic reload of config via go-d-scheduler config-reload command
    Display/browse user personal storage in user preferences
    fix pending jobs count to avoid some race conditions
    GOD-38 Add remote applications token management
    GOD-37 Support data requirements in tasks
    Add new HTTP authorization mode to manages tasks by a remote application:
        - Authorization: token XXX, with XXX a remote application token
    Add *tag* query parameter to "/api/1.0/user/{id}/task" to kill all user tasks matching tag.
    GOD-44 add restart strategy in case of node failure, new task requirement field (optional) failure_policy=N
    GOD-46 let user archive a job over
1.1.1:
    Fix tmpstorage tests
    Manage "enter" keypress on login page
1.1:
    GOD-10 allow use a script with an interactive session, script executed before ssh server
    GODWEB-1 manage multiple instances for constraints

1.0.1: - add swagger API doc/godocker.json
       - use iStatus monitoring in admin interface (etcd, etc.)
       - add apikey renew in API
       - add Temporary local storage (see go-docker)
       - optional *guest* support, i.e. accepting users connecting with Google, GitHub, ... and not in system. Guest will map to a system user for execution.
1.0: first release
